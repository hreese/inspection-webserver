package main

import (
	"encoding/json"
	"errors"
	"github.com/nanmu42/limitio"
	"io"
	"log"
	"net/http"
	"os"
	"path"
	"time"
)

const (
	maxBodySize              = 1024 * 1024 * 32 // 32 MB
	timestampDirectoryFormat = "2006-01-02T15:04:05.999999999"
)

// fileExists returns true if the given file exists and is not a directory
func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

type RequestMetadata struct {
	Method        string      `json:"method"`
	Path          string      `json:"path"`
	Proto         string      `json:"proto"`
	Header        http.Header `json:"header"`
	Host          string      `json:"host"`
	RemoteAddr    string      `json:"remote_addr"`
	ContentLength int64       `json:"content_length"`
	BodyTruncated bool        `json:"body_truncated"`
}

// DumpRequest dumps the request body and metadata to disk. It implements http.HandlerFunc.
func DumpRequest(w http.ResponseWriter, r *http.Request) {
	var (
		requestOut    RequestMetadata
		outputBaseDir string
		f             *os.File
	)

	// create request timestamp
	ts := time.Now().Format(timestampDirectoryFormat)

	log.Printf("New connection from %s\n", r.RemoteAddr)

	// create output directory
	destinationHostString := r.Host
	if destinationHostString == "" {
		destinationHostString = "__hostname.unknown"
	}
	outputBaseDir = path.Join(destinationHostString, ts)
	err := os.MkdirAll(outputBaseDir, 0750)
	if err != nil {
		log.Println(err)
		w.WriteHeader(500)
		return
	}

	// process body
	f, _ = os.Create(path.Join(outputBaseDir, "body"))
	defer f.Close()
	// limit body size to prevent DoS
	limitReader := limitio.NewReader(r.Body, maxBodySize, false)
	_copyBytes, err := io.Copy(f, limitReader)
	_ = _copyBytes
	if err != nil {
		log.Println(err)
		if errors.Is(err, limitio.ErrThresholdExceeded) {
			requestOut.BodyTruncated = true
		}
		// return 500 with empty body
		w.WriteHeader(500)
	} else {
		requestOut.BodyTruncated = false
		// return 200 with empty body
		w.WriteHeader(200)
	}

	// collect metadata
	requestOut.Method = r.Method
	requestOut.Path = r.URL.String()
	requestOut.Proto = r.Proto
	requestOut.Header = r.Header
	requestOut.Host = r.Host
	requestOut.RemoteAddr = r.RemoteAddr
	requestOut.ContentLength = r.ContentLength

	// dump metadata
	f, _ = os.Create(path.Join(outputBaseDir, "metadata.json"))
	defer f.Close()
	encoder := json.NewEncoder(f)
	encoder.SetIndent("", "  ")
	err = encoder.Encode(requestOut)
	if err != nil {
		log.Println(err)
		w.WriteHeader(500)
		return
	}
}

func main() {
	var (
		listenAddr  = os.Getenv("IW_LISTEN_ADDR")
		tlsKeyFile  = os.Getenv("IW_TLS_KEY")
		tlsCertFile = os.Getenv("IW_TLS_CERT")
		useTLS      = false
	)

	// use TLS if both key and cert are set
	if fileExists(tlsKeyFile) && fileExists(tlsCertFile) {
		useTLS = true
	}

	// set default listen address
	if listenAddr == "" {
		if useTLS {
			listenAddr = ":8443"
		} else {
			listenAddr = ":8080"
		}
	}

	// start server
	if useTLS {
		log.Printf("Listening on %s with TLS\n", listenAddr)
		log.Println(
			http.ListenAndServeTLS(listenAddr, tlsCertFile, tlsKeyFile, http.HandlerFunc(DumpRequest)))
	} else {
		log.Printf("Listening on %s without TLS\n", listenAddr)
		log.Println(
			http.ListenAndServe(listenAddr, http.HandlerFunc(DumpRequest)))
	}
}
