# inspection-webserver

`inspection-webserver` is a webserver for debugging or inspecting unknown incoming HTTP(S) requests.
This was initially built to understand undocumented webhooks from a cloud provider.

Every request is dumped into a directory named after the request timestamp
with nanosecond accuracy (e.g. `2025-06-21T15:23:18.487150833/`) inside a directory
named after hostname:port (e.g. `test.example.com:4444/`) from the request.

Every request dump consists of two files:
* `body`: The complete and unaltered request body
* `metadata.json`: Contains these keys: 
  * `method`: HTTP verb (e.g. `GET`, `POST`, `PUT`, `DELETE`)
  * `path`: Path of the request (e.g. `/api/v1/callback/xyz?c=42`).
  * `proto`: HTTP protocol version (e.g. `HTTP/1.1`).
  * `header`: All HTTP headers
  * `host`: Hostname and port from the request (e.g. `test.example.com:4444`).
  * `remote_addr`: IP address and port of the client (e.g. `1.2.3.4:54862`).
  * `content_length`: Length of the request body in bytes.
  * `body_truncated`: Set to `true` if the request body was truncated (current request size limit is 32 MiB). 

## Building from source

Clone repository, [install the go compiler](https://go.dev/doc/install) and run `go build`.

## Usage

`inspection-webserver` is configured via environment variables:

* `IW_LISTEN_ADDR`: Address and port to listen to. Defaults to `:8080` (HTTP) or `:8443` (HTTPS).
* `IW_TLS_CERT`: Path to TLS certificate.
* `IW_TLS_KEY`: Path to TLS key.

If both `IW_TLS_CERT` and `IW_TLS_KEY` are set to existing files,
`inspection-webserver` will speak TLS/HTTPS.
Otherwise, it will speak plain HTTP.